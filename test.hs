"bohyun"
--Q1 add_and_double function: add two numbers and double them, with test add_and_double 5 4. Test result:18
add_and_double :: (Num a)=>a->a->a
add_and_double x y = (x+y)*2
--Q2 (+*) function: same as add_and_double, with test (+*) 5 4. Test result:18
(+*) :: (Num a)=>a->a->a
(+*) x y = x `add_and_double` y
--Q3 solve_quadratic_equation:like solving equaction, get two answers x and y, with test solve_quadratic_equation 1 6 8. Test result:(-2.0,-4.0)
solve_quadratic_equation:: Double -> Double -> Double -> (Double,Double)        
solve_quadratic_equation a b c = (x, y)
                        where x = e + sqrt d / (2 * a)
                              y = e - sqrt d / (2 * a)
                              d = b * b - 4 * a * c
                              e = - b / (2 * a)	
--Q4 first_n function:n is the value of the list, and every list start from 1, with test first_n 5. Test result:[1,2,3,4,5]
first_n :: Int -> [Int] 
first_n (n) = take n [1 ..]
--Q5 first_n_integers function with test  first_n_integers 2. Test result:[1,2]. Reference book: Beginner of Functional Programming.
first_n_integers :: Integer -> [Integer]
first_n_integers a = take_integer (a, [1 ..])
	where 
	take_integer :: (Integer, [Integer]) -> [Integer]
	take_integer (n, list)
		| (n < 0) = error "(n < 0)" 
		| (n == 0) = [] 
		| (list == []) = [] 
		| otherwise = (head (list) : take_integer ((n-1), (tail list)))
--Q6 double_factorial function: multiplyall the integers, with test double_factorial 5. Test result:34560.Reference book: Beginner of Functional Programming.
double_factorial :: Integer -> Integer
double_factorial n 
	|n==1 = 1
	|otherwise = factorial(n) * double_factorial (n-1)
          	where
		factorial :: Integer -> Integer
		factorial n 
			|n==1 = 1
			|otherwise = n * factorial (n-1)
--Q7 factorials function using zipWith without test.(Don't know how to do this).Reference book: Beginner of Functional Programming.
factorials :: [Integer]
factorials = 1:zipWith  ( * ) factorials [1..]	
--Q8 isPrime function: define the number if is prime, test with isPrime 5 and isPrime 6. Test result: True and False.
isPrime :: Integer->Bool
isPrime n
      | n<1=False
	  | n==2=True
	  | n `mod` 3==0=False
	  | otherwise=True
--Q9 (Doesn't work)
primes :: [Integer]
primes = filter isPrime[0..]
--Q10 sum function: add all the elements from the list, test with sum'[2,5,6]. Test result: 13
sum' :: [Integer]->Integer
sum' [] = 0
sum' (x:xs) = x + sum xs
--Q11 sum function: same as sum, use foldl test with sum1'[2,5,6,8].Test result: 21
sum1' :: [Int]->Int
sum1' = foldl (+)0
--Q12 multiply function: multiply all the elements from the list, in term of foldr test with multiply[5,6,7,9].Test result:1890
multiply :: [Int]->Int
multiply = foldr (\n1-> \n2->n1*n2)1
--Q13 Don't know how to do that, yet.
--Q14 dotProduct function: do(x1,x2)(y1,y2)=x1*y1+x2*y2, test with dotProduct [3,5][4,2]. Test result:22
dotProduct :: Num a => [a] -> [a] -> a
dotProduct xs ys = foldl (+) 0 (zipWith (*) xs ys)
--Q15 is_even function: check the number if is even number, test with is_even 5 and is_even 6. Test result: False and True
is_even :: Integral a => a -> Bool
is_even n
      | n `mod` 2 == 0 = True
	  | otherwise = False
--Q16 unixname function test with unixname "the house". Test result:"th hs".
unixname :: String ->String
unixname [] = []
unixname (x:xs)
  |not( x `elem` "aeiou") = x: unixname xs
  |otherwise = unixname xs
--Q17 intersection function: get the same elements from two lists, test with intersection[1,2,3,4,5][3,4,5,6,7]. Test result:[3,4,5]
intersection :: (Eq a) => [a] -> [a] -> [a]
intersection xs ys = let ns = [ x | x <- xs, elem x ys] in [ y | y <- ys, elem y ns]
--Q18 censor function with test censor "the house". Test result:"thx hxxsx".
censor :: String -> String 
censor [] =[]
censor (x:xs)
  |not( x `elem` "aeiou" ) = x: censor xs
  |otherwise = 'x':censor xs
 
"bohyun"
